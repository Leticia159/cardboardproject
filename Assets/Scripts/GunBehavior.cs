﻿using UnityEngine;
using System.Collections;

public class GunBehavior : MonoBehaviour 
{

	public float fireRateTime;
	private float currentFireRatetime;
	private bool canFire = true;

	public int capacityAmmo, capacityAmmoOutGun;
	private int currentAmmo, currentAmmoOutGun;

	public BulletBehavior bullet;
	public Transform positionSpawn;

	public Animator animator;
	public AudioSource fireSound;

	void Start () 
	{
		currentAmmo = capacityAmmo;
		currentAmmoOutGun = capacityAmmoOutGun;
	}
	
	void Update ()
	{
		currentFireRatetime += Time.deltaTime;
		if (currentFireRatetime > fireRateTime) 
		{
			canFire = true;
		} 
		else 
		{
			canFire = false;
		}
	}

	//Balas infinitas por enquanto;
	public void Fire()
	{
		//if (currentAmmo >= 0)
		//{
		if (canFire)
		{
			Instantiate (bullet.gameObject, positionSpawn.position, transform.rotation);
			animator.SetTrigger ("Fire");
			fireSound.Play ();
			currentFireRatetime = 0;
		}
		//	currentAmmo--;
		//}
		//else
	//	{
		//	currentAmmo += currentAmmoOutGun;
		//	currentAmmoOutGun = 0;
	//	}
	}

	/*
	public void Reload()
	{
		int needAmmo = capacityAmmo - currentAmmo;
		if (currentAmmoOutGun > needAmmo) {
			currentAmmoOutGun -= needAmmo;
			currentAmmo += needAmmo;
		}
	}
	*/

}

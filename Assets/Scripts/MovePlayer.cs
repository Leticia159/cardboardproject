﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour 
{
	private CharacterController player;
	public float speed;
	public bool canWalk = true;

	// Use this for initialization
	void Start ()
	{
		player = this.GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (canWalk)
		{
			player.Move (transform.forward * speed * Time.fixedDeltaTime);
		}
		
	}
}

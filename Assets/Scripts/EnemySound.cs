﻿using UnityEngine;
using System.Collections;

public class EnemySound : MonoBehaviour
{
	public AudioSource[] audioClip;
	private int randomSound;


	void Start()
	{
		InvokeRepeating ("Sound", 0, Random.Range(2,5));
	}

	public void Sound()
	{
		randomSound = Random.Range (0, 5);
		if (randomSound == 0)
		{
			audioClip[0].Play();
		}

		if (randomSound == 5) {
			audioClip [1].Play ();
		}
	}
}

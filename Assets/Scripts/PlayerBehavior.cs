﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerBehavior : MonoBehaviour
{
	private MovePlayer movePlayer;
	public GunBehavior gun;
	public int health;
	public AudioSource audioClip;
	private bool playAudio;

	public Image[] blood;
	public Image bloodBorder;
	public GameObject dyingImage;

	private Image image;

	void Start()
	{
		
		movePlayer = GetComponent<MovePlayer> ();
	}

	void Update () 
	{
		RaycastHit hit;
		Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0));
		if (Physics.Raycast (ray, out hit, 20))
		{
			if (hit.collider.CompareTag ("Zombie")) 
			{
				gun.Fire ();
				movePlayer.canWalk = false;
			}
		}
				
		if(Input.GetButtonDown("Fire1"))
		{
			gun.Fire ();
		}



	
	}

	public void GetHit(int damage)
	{
		health -= damage;
		image = blood[Random.Range (0, blood.Length)];
		//image.enabled = false;
		Blood ();
		if (health < 15)
		{
			bloodBorder.enabled = true;
		}
		Die ();
	}

	void Die()
	{
		if(health <= 0)
		{
			if (!playAudio) 
			{
				dyingImage.SetActive (true);
				audioClip.Play ();
				StartCoroutine ("ChangeScene");
				playAudio = true;
			}
		}
	}

	IEnumerator ChangeScene()
	{
		yield return new WaitForSeconds (audioClip.clip.length);
		SceneManager.LoadScene ("GameOver");
	}


	void Blood()
	{
		image.enabled = true;
		image.CrossFadeAlpha (0.0f, 1.0f, false);
		StartCoroutine ("HideImage");
	}

	IEnumerator HideImage()
	{
		yield return new WaitForSeconds (1.0f);
		image.enabled = false;
	}

	public void ChangeCanWalk()
	{
		movePlayer.canWalk = true;
	}

}

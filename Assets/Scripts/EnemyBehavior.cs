﻿using UnityEngine;
using System.Collections;

public class EnemyBehavior : MonoBehaviour 
{
	private CharacterController controller;
	private NavMeshAgent agent;
	public float speed;

	public float range;
	public int damage;

	public int health;

	private PlayerBehavior playerBeha;
	private EnemyManager enemyManager;
	private GameObject player;
	private Animator animator;
	public AudioSource audio;

	public float timeToAttack;
	private float currentTimeToAttack;
	private bool canAttack = true;

	public float rotationSpeed;


	void Start()
	{
		
		controller = this.GetComponent<CharacterController> ();
		animator = this.GetComponent<Animator>();
		enemyManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<EnemyManager>();
		player = GameObject.FindGameObjectWithTag ("Player");
		playerBeha = GameObject.Find ("FPSController").GetComponent<PlayerBehavior> ();
		agent = GetComponent<NavMeshAgent> ();

	}

	void Update ()
	{
		
		Die ();

		currentTimeToAttack += Time.deltaTime;
		if (currentTimeToAttack > timeToAttack) 
		{	
			canAttack = true;
		} else {
			canAttack = false;
		}

		if (!InRange ())
		{
			Chase ();
		}

		if (InRange() && canAttack) 
		{
			animator.SetBool ("andar", false);
			//Attack ();
			animator.SetTrigger ("atacar");
			currentTimeToAttack = 0;
		}
	}


	void Chase()
	{transform.LookAt (player.transform.position);
		Quaternion newRotation = Quaternion.LookRotation(player.transform.position - transform.position);
		newRotation.x = newRotation.z = 0;
		transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * rotationSpeed);
		//controller.SimpleMove(transform.forward * speed);



		controller.SimpleMove (transform.forward * speed);
		//agent.SetDestination (player.transform.position);
		animator.SetBool ("andar", true);
	}

	bool InRange()
	{
		if(Vector3.Distance(transform.position, player.transform.position) < range)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void Attack()
	{
		transform.LookAt (player.transform.position);
		//animator.SetTrigger ("atacar");
		playerBeha.GetHit (damage);
		audio.Play ();

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Bullet"))
		{
			animator.SetTrigger ("getHit");
			health = health - 1;
		}
	}

	void Die()
	{
		if(health <= 0)
		{
			enemyManager.enemyCount--;
			animator.SetTrigger ("die");
			Destroy(gameObject);

		}
	}

		
}
